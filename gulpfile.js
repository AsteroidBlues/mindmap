const gulp = require('gulp');

const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const connect = require('gulp-connect');
const fancyLog = require('fancy-log');
const source = require('vinyl-source-stream');
const sourcemaps = require('gulp-sourcemaps');
const ts = require('gulp-typescript');
const tsify = require('tsify');
const watchify = require('watchify');

const project = ts.createProject('tsconfig.json');

const paths = {
    ts: ['src/dom_utils.ts', 'src/driver.ts', 'src/mindmap.ts', 'src/ui.ts'],
    pages: ['src/*.html', 'src/*.css']
};
const outdir = 'built/gulp/';
const bundleName = 'mindmap.js'

const watchedBrowserify = watchify(browserify({
    basedir: '.',
    debug: true,
    entries: paths.ts,
    cache: {},
    packageCache: {}
})).plugin(tsify);

function bundle() {
    return watchedBrowserify
        .bundle()
        .pipe(source(bundleName))
        .pipe(buffer())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(outdir));
}

watchedBrowserify.on('update', bundle);
watchedBrowserify.on('log', fancyLog);

gulp.task('bundle_ts', function() {
    return bundle()
        .pipe(connect.reload());
});

gulp.task('copy_html', function() {
    return gulp.src(paths.pages)
        .pipe(gulp.dest(outdir))
        .pipe(connect.reload());
});

gulp.task('watch_html', function() {
    gulp.watch(paths.pages, gulp.series('copy_html'));
});

gulp.task('serve_content', function() {
    connect.server({root: outdir, livereload: true, port: 8080});
});

gulp.task('bundle_ts', gulp.series('copy_html', bundle));

gulp.task('default', gulp.parallel('bundle_ts', 'watch_html', 'serve_content'));
