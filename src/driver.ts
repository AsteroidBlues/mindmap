import * as db_exalted from "./db_exalted";
import * as db_l5r from "./db_l5r";
import * as db_sample from "./db_sample";
import * as du from "./dom_utils";
import * as mm from "./mindmap";
import * as ui from "./ui";

//------------------------------------------------------------------------------
// TODO
// 1. document based "schema"
//      provide list of priority -> property mappings for sorting in the detail view
//      run by the controller?
// 2. detail view render plugin
//      provide property to render function mapping (combine with 1 to control sorting)
//      run by view? or in the controller and passed in as a prerendered child to the view?
//      exported as thunk to keep impl clean
// 4. use kind for optional filtering of suggestions?
// 5. SERIALIZATION!!!
// 6. friendly id?
// 7. deeplinks
//------------------------------------------------------------------------------

function dumpRawDataset(data: mm.RawData, element: HTMLElement): void {
    const t = element.appendChild(du.table());
    t.appendChild(du.tableHeader([du.text("id"), du.text("kind"), du.text("pc?")]));
    for (const e of data) {
        const r = du.tableRow([
            du.text(e.id),
            du.text(e.kind as string),
            du.text(String(e.pc))
        ]);
        t.appendChild(r);
    }
}

function dumpCookedDataset(data: mm.Data, element: HTMLElement): void {
    for (const id of Object.keys(data.byId)) {
        const e = data.byId[id]!;

        const block = element.appendChild(du.div());
        block.appendChild(du.text(`id: ${e.id}`));
        block.appendChild(du.newLine());

        for (let k of Object.keys(e.properties)) {
            const p = e.properties[k]!;

            switch (p.kind) {
                case "flag": {
                    block.appendChild(du.text(`${k}: true`));
                } break;
                case "link": {
                    const m = p.links.length > 1;

                    let txt = `${k}: `;
                    if (m) { txt += '[ '; }
                    for (let v of p.links) {
                        txt += `${v.value}${v.target ? '*' : ''}`;

                        if (v.tags.length > 0) {
                            txt += "( ";
                            for (const t of v.tags) {
                                txt += `${t}, `;
                            }
                            txt += ")";
                        }

                        if (m) { txt += ', '; }
                    }
                    if (m) { txt += `]`; }

                    block.appendChild(du.text(txt));
                } break;
                case "note": {
                    block.appendChild(du.text(`${k}: ${p.content}`));
                } break;
            }

            block.appendChild(du.newLine());
        }

        element.appendChild(du.horizontalRule());
    }
}

class Navman {
    public constructor(data: mm.Data, extension: mm.Extension) {
        this.data = data;
        this.extension = extension;
        this.backStack = [];
        this.forwardStack = [];
        this.current = null;
    }

    public show(e: mm.Entity): void {
        showEntity(e, this.extension, g_mainPane, this);
    }
    public showCurrent(): void {
        if (this.current) {
            showEntity(this.current, this.extension, g_mainPane, this);
        } else {
            dumpCookedDataset(this.data, g_mainPane);
        }
    }
    public go(id: string): void {
        const e = this.data.byId[id];
        if (!e) {
            throw `Invalid entity id ${id}`;
        }

        if (this.current) {
            this.backStack.push(this.current);
        }
        this.forwardStack = [];
        this.current = e;
        showEntity(e, this.extension, g_mainPane, this);

        if (this.navbar) {
            this.navbar.updateMainTab(this, e.id);
            this.navbar.selectMainTab();
        }
    }
    public back(): void {
        const e = this.backStack.pop();
        if (!e) {
            return;
        }

        if (this.current) {
            this.forwardStack.push(this.current);
        }
        this.current = e;
        showEntity(e, this.extension, g_mainPane, this);

        if (this.navbar) {
            this.navbar.updateMainTab(this, e.id);
            this.navbar.selectMainTab();
        }
    }
    public forward(): void {
        const e = this.forwardStack.pop();
        if (!e) {
            return;
        }

        if (this.current) {
            this.backStack.push(this.current);
        }
        this.current = e;
        showEntity(e, this.extension, g_mainPane, this);

        if (this.navbar) {
            this.navbar.updateMainTab(this, e.id);
            this.navbar.selectMainTab();
        }
    }
    public pinCurrent(): void {
        if (this.current && this.navbar) {
            this.navbar.pin(this.current, this.current.id);
        }
    }

    public readonly data: mm.Data;
    public readonly extension: mm.Extension;

    public navbar?: ui.Navbar;
    private readonly backStack: mm.Entity[];
    private forwardStack: mm.Entity[];
    private current: mm.Entity | null;
}

function makeNavbar(parent: HTMLElement, navman: Navman, documents: string[], selected: string): ui.Navbar {
    const navbar = ui.makeNavbar("navbar");
    navbar.goBack = () => {
        navman.back();
    };
    navbar.goForward = () => {
        navman.forward();
    };
    navbar.tabSelected = (item: unknown) => {
        if (item === navman) {
            navman.showCurrent();
        } else {
            navman.show(item as mm.Entity);
        }
    };
    navbar.tabPin = () => {
        navman.pinCurrent();
    };
    navbar.tabUnpin = (item: unknown) => {
        // do nothing
    };

    navbar.updateMainTab(navman, "Home");

    for (const d of documents) {
        navbar.addDocument(d, d === selected);
    }

    const n = navbar.render();
    parent.appendChild(n);

    return navbar;
}

function makeSidebar(data: mm.Data, parent: HTMLElement, navman: Navman): ui.NestedList {
    const sidebar = ui.makeNestedList();

    // list by id:
    const byId = ui.makeListItem("By Id:", sidebar);
    byId.sortPriority = 1;
    byId.children = ui.makeNestedList();
    for (const id of Object.keys(data.byId)) {
        const e = data.byId[id];

        ui.makeListItem(id, byId.children);
    }

    const byProperty = ui.makeListItem("By Property:", sidebar);
    byProperty.sortPriority = 2;
    byProperty.children = ui.makeNestedList();
    for (const p of Object.keys(data.byProperty)) {
        if (navman.extension.hideInSidebar[p]) {
            continue;
        }
        
        const valuesByProperty = data.byProperty[p];
        const pi = ui.makeListItem(p, byProperty.children);

        if (p == "kind") {
            pi.sortPriority = -1;
        }

        pi.children = ui.makeNestedList();
        for (const v of Object.keys(valuesByProperty)) {
            const values = valuesByProperty[v]!;
            if (v === "true") {
                for (const v of values) {
                    ui.makeListItem(v.id, pi.children);
                }
            } else {
                const vi = ui.makeListItem(v, pi.children);

                vi.children = ui.makeNestedList();
                for (const v of values) {
                    ui.makeListItem(v.id, vi.children);
                }
            }
        }
    }

    ui.setListClickHandler(sidebar, (e: ui.ListEntry) => {
        navman.go(e.display);
    });


    parent.appendChild(
        ui.renderNestedList(sidebar, "sidebar", ["mainPanelCol1"])
    );

    return sidebar;
}

let g_currentEntity: mm.Entity;
function showEntity(
    entity: mm.Entity,
    extension: mm.Extension,
    parent: HTMLElement,
    navman: Navman
) {
    if (g_currentEntity == entity) {
        return;
    }

    du.removeChildren(parent);

    const panel = ui.makeEntryDetails(entity.id, "main");
    for (const pid of Object.keys(entity.properties)) {
        const p = entity.properties[pid]!;

        const property = ui.makeEntryProperty(pid);
        const priority = extension.propertyToPriority(pid);
        if (priority === "ignore") {
            continue;
        }

        property.sortPriority = priority;

        const navigator = (id: string) => navman.go(id);
        const cr = extension.customRenderers[pid];
        if (cr) {
            property.customRenderer = () => cr(p, entity, navigator);
        }

        switch (p.kind) {
            case "flag": {
                // nothing
            } break;
            case "link": {
                property.values = [];
                for (const l of p.links) {
                    const v = makeEntityPropertyValue(navman, l);
                    property.values.push(v);
                }
            } break;
            case "note": {
                // nothing
            } break;
        }

        panel.addProperty(property);
    }
    for (const bid of Object.keys(entity.backLinks)) {
        const b = entity.backLinks[bid]!;

        const link = ui.makeEntryProperty(bid);
        link.values = [];
        for (const l of b.links) {
            const v = makeEntityPropertyValue(navman, l);
            link.values.push(v);
        }

        panel.addBacklink(link);
    }

    panel.onPropertyChanged = (change: string, property: ui.Property) => {
        if (change === "added") {
            console.log(`Added property '${property.display}' on ${entity.id}`);

            mm.addFlag(entity, property.display, true, navman.data);
        } else if (change === "removed") {
            console.log(`Removed property '${property.display}' from ${entity.id}`);

            mm.removeProperty(entity, property.display, navman.data);
        } else if (change === "valueChanged") {
            console.log(`Changed property: '${property.display}' on ${entity.id}`);
        }
    };

    parent.appendChild(panel.render());
}

function makeEntityPropertyValue(navman: Navman, l: mm.Link): ui.PropertyValue {
    const v: ui.PropertyValue = {
        value: l.value,
        valueExtras: [],
        tags: [],
    };

    if (l.target) {
        const kind = l.target.properties.kind;
        if (kind && kind.kind === "link" && kind.links.length > 0) {
            for (const e of kind.links) {
                v.valueExtras.push(e.value);
            }
        }

        v.goToTarget = () => {
            navman.go(l.value);
        }
    }

    for (const t of l.tags) {
        v.tags.push(t);
    }

    return v;
}

//------------------------------------------------------------------------------
// Document management
//------------------------------------------------------------------------------

interface Document {
    rawData: mm.RawData;
    extension?: mm.ExtensionDefinition;
}

function selectDocument(
    rawData: mm.RawData,
    extensionDefinition: mm.ExtensionDefinition | undefined,
    selected: string,
    documents: string[],
    documentSelected: (name: string) => void
) {
    du.removeChildren(g_navbarPane);
    du.removeChildren(g_sidebarPane);
    du.removeChildren(g_mainPane);

    const data = mm.buildData(rawData);
    const ext = mm.buildExtension(extensionDefinition);

    const navman = new Navman(data, ext);

    const navbar = makeNavbar(g_navbarPane, navman, documents, selected);
    const sidebar = makeSidebar(data, g_sidebarPane, navman);

    navman.navbar = navbar;

    navbar.documentSelected = documentSelected;

    dumpCookedDataset(data, g_mainPane);
}

function switchDocument(name: string) {

    const doc = g_documents[name];
    if (doc) {
        selectDocument(doc.rawData, doc.extension, name, Object.keys(g_documents), switchDocument);
    }
}

const element = document.getElementById("content");
if (!element) {
    throw "Cannot find root element in page";
}

const g_window = element.appendChild(du.div(undefined, undefined, ["core", "window"]));
const g_navbarPane = g_window.appendChild(du.div(undefined, undefined, ["navbar"]));
const g_sidebarPane = g_window.appendChild(du.div(undefined, undefined, ["sidebar"]));
const g_mainPane = g_window.appendChild(du.div(undefined, undefined, ["main"]));

g_navbarPane.style.gridColumn = "1 / span 2";
g_navbarPane.style.gridRow = "1 / span 1";
g_sidebarPane.style.gridColumn = "1 / span 1";
g_sidebarPane.style.gridRow = "2 / span 1";
g_mainPane.style.gridColumn = "2 / span 1";
g_mainPane.style.gridRow = "2 / span 1";

const g_documents: {[k: string]: Document | undefined } = {
    "Exalted": { rawData: db_exalted.raw, extension: db_exalted.extension },
    "L5R": { rawData: db_l5r.raw, extension: db_l5r.extension },
    "Sample": { rawData: db_sample.raw, extension: db_sample.extension },
    "Sample2": { rawData: db_sample.raw, extension: db_sample.extension },
}

switchDocument("L5R");
