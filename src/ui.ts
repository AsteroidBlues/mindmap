import * as du from "./dom_utils";

function addCssBase(e: HTMLElement, cssBase: string, suffix: string): void {
    e.classList.add(`${cssBase}${suffix}`);
}

function removeCssBase(e: HTMLElement, cssBase: string, suffix: string): void {
    e.classList.remove(`${cssBase}${suffix}`);
}

interface Sortable {
    display: string,
    sortPriority: number,
}

function elementSorter(l: Sortable, r: Sortable): number {
    if (l.sortPriority == r.sortPriority) {
        return l.display.localeCompare(r.display);
    } else {
        return l.sortPriority - r.sortPriority;
    }
}

//------------------------------------------------------------------------------
// Control
//------------------------------------------------------------------------------
export interface Control {
    render: () => HTMLElement,
}

//------------------------------------------------------------------------------
// Entry Details
//------------------------------------------------------------------------------
export class EntryDetails  implements Control {
    // Control
    public render(): HTMLDivElement {
        if (!this.is_dirty) {
            return this.lazy_element!;
        }

        if (!this.lazy_element) {
            this.lazy_element = du.div();
            addCssBase(this.lazy_element, this.cssBase, "_entryDetails");

            if (this.setTitle) {
                this.setTitle(this.id);
            } else {
                const eid = this.lazy_element.appendChild(du.div());
                addCssBase(eid, this.cssBase, "_entryDetails_id");
                eid.appendChild(du.text(this.id));
            }

            this.lazy_properties = this.lazy_element.appendChild(du.div());
            addCssBase(this.lazy_properties, this.cssBase, "_entryDetails_properties");

            /* new prop ui
            this.lazy_newproperty = du.div();
            this.lazy_newproperty.style.gridColumn = "3 / span 1";
            this.lazy_newproperty.style.gridRow = `${-2} / span 1`;

            const newProp = this.lazy_newproperty.appendChild(du.textBox());
            addCssBase(newProp, this.cssBase, "_entryDetails_newProp")

            newProp.onkeypress = (event: KeyboardEvent) => {
                if (event.key === "Enter") {
                    const newPropName = newProp.value.trim();
                    if (newPropName === "") {
                        return false;
                    }

                    const p = makeEntryProperty(newPropName);
                    p.sortPriority = this.properties.length;
                    this.addProperty(p);
                    this.render();

                    newProp.value = "";
                    newProp.focus();
                    return false;
                }
            };
            /**/

            this.lazy_element.appendChild(du.horizontalRule());
            const backlinks = this.lazy_element.appendChild(du.div());
            addCssBase(backlinks, this.cssBase, "_entryDetails_backlinks");

            const gridRows = this.backlinks.length;
            backlinks.style.gridTemplateRows = `repeat(${gridRows}, auto)`;

            const sortedBacklinks = this.backlinks.slice();
            sortedBacklinks.sort(elementSorter);
            for (let i = 0; i < sortedBacklinks.length; ++i) {
                const b = sortedBacklinks[i];

                const rowStyle = `${i + 1} / span 1`; // 1 based indexing

                const bn = backlinks.appendChild(du.div([du.text(`${b.display} of`)]));
                bn.style.gridRow = rowStyle;
                bn.style.gridColumn = "2 / span 1";
                addCssBase(bn, this.cssBase, "_entryDetails_backlink_name");

                const bvs = backlinks.appendChild(du.div());
                bvs.style.gridRow = rowStyle;
                bvs.style.gridColumn = "3 / span 1";
                addCssBase(bvs, this.cssBase, "_entryDetails_backlink_value");

                if (b.values) {
                    this.renderPropertyValues(b.values, bvs);
                }
            }
        }

        const eps = this.lazy_properties!;
        du.removeChildren(eps);

        const gridRows = this.properties.length; //+ 1; // +1 for the new item entry
        eps.style.gridTemplateRows = `repeat(${gridRows}, auto)`;

        /* new prop ui
        eps.appendChild(this.lazy_newproperty!);
        /**/

        const sortedProps = this.properties.slice();
        sortedProps.sort(elementSorter);
        for (let i = 0; i < sortedProps.length; ++i) {
            const p = sortedProps[i];

            if (!p.lazy_elements) {
                p.lazy_elements = [];

                let render = true;
                if (p.customRenderer) {
                    const custom = p.customRenderer();
                    if (custom) {
                        p.lazy_values = du.div([custom]);
                        p.lazy_values.style.gridColumn = "3 / span 1";
                        addCssBase(p.lazy_values, this.cssBase, "_entryDetails_property_value");
                        p.lazy_elements.push(p.lazy_values);
                        render = false;
                    }
                }

                if (render) {
                    /* don't render the delete button
                    p.lazy_button = du.button();
                    p.lazy_button.style.gridColumn = "1 / span 1";
                    p.lazy_elements.push(du.div([p.lazy_button]));
                    /**/

                    const epn = du.div([du.text(p.display)]);
                    epn.style.gridColumn = "2 / span 1";
                    addCssBase(epn, this.cssBase, "_entryDetails_property_name");
                    p.lazy_elements.push(epn);

                    p.lazy_values = du.div();
                    p.lazy_values.style.gridColumn = "3 / span 1";
                    addCssBase(p.lazy_values, this.cssBase, "_entryDetails_property_value");
                    p.lazy_elements.push(p.lazy_values);

                    this.renderPropertyValues(p.values, p.lazy_values);
                }

                const rowStyle = `${i + 1} / span 1`; // 1 based indexing
                for (const le of p.lazy_elements) {
                    addCssBase(le, this.cssBase, "_entryDetails_property");
                    le.style.gridRow = rowStyle;
                }
            }

            for (const le of p.lazy_elements) {
                eps.appendChild(le);

                if (p.uiState === "adding") {
                    p.uiState = "added";
                    le.classList.remove(`${this.cssBase}_entryDetails_property_removed`);
                    /* don't render the delete button
                    p.lazy_button!.innerText = "-";
                    p.lazy_button!.onclick = (ev: MouseEvent) => {
                        this.removeProperty(p);
                        this.render();
                    }
                    */
                } else if (p.uiState === "removing") {
                    p.uiState = "removed";
                    le.classList.add(`${this.cssBase}_entryDetails_property_removed`);
                    /* don't render the delete button
                    p.lazy_button!.innerText = "+";
                    p.lazy_button!.onclick = (ev: MouseEvent) => {
                        this.addProperty(p);
                        this.render();
                    }
                    */
                }
            }
        }

        // TODO first entry

        this.is_dirty = false;
        return this.lazy_element;
    }

    renderPropertyValues(values: PropertyValue[], slot: HTMLElement) {
        const sortedValues = values.slice();
        sortedValues.sort();
        for (const v of sortedValues) {
            /* don't do a text box
            const vbox = vdiv.appendChild(du.textBox(v.value));
            addCssBase(vbox, this.cssBase, "_entryDetails_property_value");
            */

            if (v.valueExtras.length > 0) {
                let extras = " ";
                for (const e of v.valueExtras) {
                    extras = extras.concat(`${e}, `);
                }
                slot.appendChild(du.text(`${v.value} (${extras})`));
            } else {
                slot.appendChild(du.text(v.value));
            }
            if (v.goToTarget) {
                slot.appendChild(du.text(" "));
                slot.appendChild(du.button("→", () => { v.goToTarget!(); }));
            }
            slot.appendChild(du.newLine());

            if (v.tags.length > 0) {
                const sortedTags = v.tags.slice();
                sortedTags.slice();

                const ts = slot.appendChild(du.paragraph());
                ts.style.textIndent = "15px";
                ts.style.margin = "0px";

                for (const t of sortedTags) {
                    ts.appendChild(du.text(`${t}, `));
                }
            }
        }
    }

    public constructor(id: string, cssBase: string) {
        this.id = id;
        this.properties = [];
        this.backlinks = [];
        this.cssBase = cssBase;
        this.is_dirty = true;
    }

    public addProperty(p: Property): void {
        p.uiState = "adding";
        if (this.properties.indexOf(p) < 0) {
            this.properties.push(p);
        }

        if (this.onPropertyChanged) {
            this.onPropertyChanged("added", p);
        }

        this.is_dirty = true;
    }

    public removeProperty(p: Property): void {
        p.uiState = "removing";

        if (this.onPropertyChanged) {
            this.onPropertyChanged("removed", p);
        }

        this.is_dirty = true;
    }

    public addBacklink(p: Property): void {
        p.uiState = "added";
        this.backlinks.push(p);
    }

    public onPropertyChanged?: (change: "added" | "removed" | "valueChanged", property: Property) => void;
    public setTitle?: (display: string) => void;

    private readonly id: string;
    private readonly properties: Property[];
    private readonly backlinks: Property[];

    private readonly cssBase: string;
    private lazy_element?: HTMLDivElement;
    private is_dirty: boolean;

    private lazy_properties?: HTMLDivElement;
    private lazy_newproperty?: HTMLDivElement;
}

export interface Property {
    display: string,
    sortPriority: number,
    values: PropertyValue[],

    customRenderer?: CustomRenderer,

    uiState: "adding" | "added" | "removing" | "removed";
    lazy_elements?: HTMLDivElement[];
    lazy_button?: HTMLButtonElement;
    lazy_values?: HTMLDivElement;
}

export interface PropertyValue {
    value: string,
    valueExtras: string[],
    tags: string[],
    goToTarget?: () => void,
}

export type CustomRenderer = () => HTMLElement | null;

export function makeEntryDetails(id: string, cssBase: string): EntryDetails {
    return new EntryDetails(id, cssBase);
}

export function makeEntryProperty(display: string): Property {
    return { display: display, sortPriority: 0, values: [], uiState: "removed" };
}

//------------------------------------------------------------------------------
// Navbar
//------------------------------------------------------------------------------
export class Navbar implements Control {
    // Control
    public render(): HTMLTableElement {
        if (!this.is_dirty) {
            return this.lazy_element!;
        }

        if (!this.lazy_element) {
            this.lazy_element = du.table();
            this.cleanStyle(this.lazy_element);
            this.setSize(this.lazy_element, 100, 100);

            const b = this.lazy_element.appendChild(du.tableRow([]));
            this.cleanStyle(b);

            const navigationCell = b.appendChild(du.tableRowCell());
            const tabCell = b.appendChild(du.tableRowCell());
            this.cleanStyle(navigationCell);
            this.setSize(navigationCell, 20, 100);
            this.cleanStyle(tabCell);
            this.setSize(tabCell, 80, 100);

            const navigation = navigationCell.appendChild(du.table());
            this.cleanStyle(navigation);
            this.setSize(navigation, 100, 100);

            this.lazy_selector = du.select();
            addCssBase(this.lazy_selector, this.cssBase, "_selector");
            this.lazy_selector.onchange = () => {
                this.selectedDocument = this.lazy_selector!.selectedIndex;
                if (this.documentSelected) {
                    this.documentSelected(this.documents[this.selectedDocument]);
                }
            }

            const back = du.button("<", () => {
                if (this.goBack) {
                    this.goBack();
                }
            });
            const forward = du.button(">", () => {
                if (this.goForward) {
                    this.goForward();
                }
            });
            addCssBase(back, this.cssBase, "_back");
            addCssBase(forward, this.cssBase, "_forward");

            const navigationRow = navigation.appendChild(du.tableRow([]));
            const selectorCell = navigationRow.appendChild(du.tableRowCell(this.lazy_selector));
            const backCell = navigationRow.appendChild(du.tableRowCell(back));
            const forwardCell = navigationRow.appendChild(du.tableRowCell(forward));
            this.cleanStyle(navigationRow);
            this.cleanStyle(selectorCell);
            this.setSize(selectorCell, 75, 100);
            this.cleanStyle(backCell);
            this.setSize(backCell, 12.5, 100);
            this.cleanStyle(forwardCell);
            this.setSize(forwardCell, 12.5, 100);

            const tabs = tabCell.appendChild(du.table());
            this.cleanStyle(tabs);
            this.setSize(tabs, 100, 100);
            tabs.style.paddingLeft = "5px";
            tabs.style.paddingRight = "5px";

            this.lazy_tabs = tabs.appendChild(du.tableRow([]));
            addCssBase(this.lazy_tabs, this.cssBase, "_tabs");
        }
        const b = this.lazy_element;

        const sel = this.lazy_selector!;
        du.removeChildren(sel);

        for (const d of this.documents) {
            sel.appendChild(du.option(d));
        }
        sel.selectedIndex = this.selectedDocument;

        const ts = this.lazy_tabs!;
        du.removeChildren(ts);

        const mainTab = this.tabs[0];

        const sortedTabs = this.tabs.slice();
        sortedTabs.sort(elementSorter);
        for (const t of sortedTabs) {
            if (!t.lazy_element) {
                const content = du.span();
                content.appendChild(du.text(t.display));
                content.appendChild(du.button(t === mainTab ? "📌" : "X", () => {
                    if (t === mainTab) {
                        if (this.tabPin) {
                            this.tabPin(t.item);
                        }
                    } else {
                        if (this.tabUnpin) {
                            this.tabUnpin(t.item);
                            this.unpin(t);
                        }
                    }
                }));

                t.lazy_element = du.tableRowCell(content);
                t.lazy_element.onclick = () => {
                    this.selectTab(t);
                }

                let cssSuffix = "_tab_pinned";
                if (t === mainTab) {
                    cssSuffix = "_tab_main";
                }

                addCssBase(t.lazy_element, this.cssBase, cssSuffix);
            }

            if (t === this.selectedTab) {
                addCssBase(t.lazy_element, this.cssBase, "_tab_selected");
            } else {
                removeCssBase(t.lazy_element, this.cssBase, "_tab_selected");
            }

            ts.appendChild(t.lazy_element);
        }
        const pad = ts.appendChild(du.tableRowCell());
        //pad.style.width = "100%";

        this.is_dirty = false;
        return b;
    }

    public constructor(cssBase: string) {
        this.documents = [];
        this.selectedDocument = 0;
        this.tabs = [];

        this.tabs.push({ item: null, display: "", sortPriority: -1 });
        this.selectedTab = this.tabs[0];

        this.cssBase = cssBase;
        this.is_dirty = true;
    }

    public updateMainTab(item: unknown, display: string): void {
        rebindTab(this.tabs[0], item, display);
        this.is_dirty = true;

        this.render();
    }

    public addDocument(display: string, select?: boolean): void {
        this.documents.push(display);
        this.is_dirty = true;

        if (select) {
            this.selectedDocument = this.documents.length - 1;
        }
    }

    public pin(item: unknown, display: string): void {
        this.tabs.push({ item: item, display: display, sortPriority: 0 });
        this.is_dirty = true;
        this.render();
    }
    private unpin(t: Tab): void {
        this.tabs.splice(this.tabs.indexOf(t), 1);
        this.is_dirty = true;
        this.render();
    }

    public selectMainTab(): void {
        this.selectTab(this.tabs[0]);
    }
    private selectTab(t: Tab): void {
        if (this.selectedTab === t) {
            return;
        }

        if (this.tabSelected) {
            this.tabSelected(t.item);
        }

        this.selectedTab = t;

        this.is_dirty = true;
        this.render();
    }

    public documentSelected?: (name: string) => void;
    public goBack?: () => void;
    public goForward?: () => void;

    public tabSelected?: (item: unknown) => void;

    public tabPin?: (item: unknown) => void;
    public tabUnpin?: (item: unknown) => void;

    private setSize(e: HTMLElement, width: number, height: number): void {
        e.style.height = `${height}%`;
        e.style.width = `${width}%`;
    }

    private cleanStyle(e: HTMLElement): void {
        e.style.borderSpacing = "0px";
        e.style.padding = "0px";
        e.style.verticalAlign = "center";
    }

    private readonly documents: string[];
    private selectedDocument: number;
    private readonly tabs: Tab[];
    private selectedTab: Tab;
    private readonly cssBase: string;
    private lazy_element?: HTMLTableElement;
    private is_dirty: boolean;

    private lazy_selector?: HTMLSelectElement;
    private lazy_tabs?: HTMLTableRowElement;
}

interface Tab {
    item: unknown;
    display: string;
    sortPriority: number,

    lazy_element?: HTMLTableCellElement,
}

function rebindTab(tab: Tab, item: unknown, display: string): void {
    tab.item = item;
    tab.display = display;
    tab.lazy_element = undefined;
}

export function makeNavbar(cssBase: string): Navbar {
    return new Navbar(cssBase);
}

//------------------------------------------------------------------------------
// Nested List
//------------------------------------------------------------------------------
export interface NestedList {
    items: ListEntry[],
    lazy_element?: HTMLUListElement,
}

export interface ListEntry {
    display: string,
    sortPriority: number,
    children?: NestedList,
    lazy_element?: HTMLLIElement,
    click_handler?: (i: ListEntry) => void,
}

export function makeNestedList(parent?: ListEntry): NestedList {
    return { items: [] };
}

export function makeListItem(display: string, parent: NestedList): ListEntry {
    const i = { display: display, sortPriority: 0 };
    parent.items.push(i);
    return i;
}

export function setListClickHandler(list: NestedList, handler: (i: ListEntry) => void): void {
    for (const c of list.items) {
        c.click_handler = handler;

        if (c.children) {
            setListClickHandler(c.children, handler);
        }
    }
}

export function renderNestedList(list: NestedList, cssBase: string, cssClasses?: string[]): HTMLUListElement {
    if (!list.lazy_element) {
        list.lazy_element = du.list(cssClasses);
        list.lazy_element.classList.add(`${cssBase}_ul`);
    }
    const e = list.lazy_element;

    const items = list.items.slice();
    items.sort(elementSorter);

    for (const i of items) {
        if (!i.lazy_element) {
            i.lazy_element = du.listElement(cssClasses);
            i.lazy_element.classList.add(`${cssBase}_li`);

            const ic = i.lazy_element.appendChild(du.div());
            let icon: HTMLSpanElement;
            if (i.children) {
                icon = ic.appendChild(du.text("-"));
                addCssBase(icon, cssBase, "_li_icon");
            }
            let text = ic.appendChild(du.text(i.display));
            if (i.children) {
                addCssBase(text, cssBase, "_li_heading");
            }
            ic.onclick = () => {
                if (i.children && i.children.lazy_element) {
                    if (i.children.lazy_element.hidden) {
                        icon.innerText = `-`;
                    } else {
                        icon.innerText = `+`;
                    }
                    i.children.lazy_element.hidden = !i.children.lazy_element.hidden;
                } else if (i.click_handler) {
                    i.click_handler(i);
                }
            }
        }
        const ie = i.lazy_element;

        if (i.children) {
            const subList = renderNestedList(i.children, cssBase, cssClasses);
            ie.appendChild(subList);
        }

        e.appendChild(ie);
    }

    return e;
}
