import * as mm from "./mindmap";

export const raw: mm.RawData = [
    {
        id: "Koyomi",
        kind: "person",
        exaltation: "Solar",
        caste: "Night",
        pc: true,
        ties: {
            "Lilly": ["respect", "major"],
            "Rook": ["save him from himself", "minor"],
        },
        principles: {
            "I will pee in Lord Malice's eyesocket": "major",
            "I am the chosen bearer of the weapons of endings": true,
        }
    },
    {
        id: "Lilly",
        kind: "person",
        exaltation: "Solar",
        caste: "Eclipse",
        pc: true,
        ties: ["Koyomi", "Rook", "Vanessa"],
    },
    {
        id: "Rook",
        kind: "person",
        exaltation: "Solar",
        caste: "Twilight",
        pc: true,
        ties: "Lilly",
    },
    {
        id: "Vanessa",
        kind: "person",
        exaltation: "Lunar",
        caste: "Full Moon",
        pc: false
    },
    {
        id: "Solar",
        kind: "exaltation",
    },
    {
        id: "Lunar",
        kind: "exaltation",
    },
    {
        id: "Night",
        kind: "caste",
    },
    {
        id: "Twilight",
        kind: "caste",
    },
    {
        id: "Full Moon",
        kind: "caste",
    }
];

export const extension: mm.ExtensionDefinition = {
    propertySorting: {
        "-1": "kind",
        "2": "pc",
    },
    hideInSidebar: [
    
    ],
    customRenderers: {

    },
};
