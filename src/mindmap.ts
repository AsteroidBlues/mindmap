import { renderNestedList } from "./ui";

//------------------------------------------------------------------------------
// Raw types
//------------------------------------------------------------------------------

export type RawTaggedProperties = { [v: string]: boolean | string | ReadonlyArray<string> };
export type RawProperty = boolean | string | ReadonlyArray<string> | RawTaggedProperties;
export interface RawEntity {
    readonly id: string;
    readonly [property: string]: RawProperty;
}
export type RawData = ReadonlyArray<RawEntity>;

//------------------------------------------------------------------------------
// Type assertions

export function isRawProperty(p: RawProperty): p is RawProperty {
    if (p === undefined || p === null) {
        return false;
    } else if (typeof p === "boolean") {
        return true;
    } else if (typeof p === "string") {
        return true;
    } else if (p instanceof Array) {
        for (const pv of p) {
            if (typeof pv !== "string") {
                return false;
            }
        }
    }

    return true;
}

export function isRawEntity(e: RawEntity): e is RawEntity {
    if (e === undefined || e === null) {
        return false;
    }

    for (const k of Object.keys(e)) {
        if (!isRawProperty(e[k])) {
            return false;
        }
    }

    if (typeof e.id !== "string") {
        return false;
    }

    return true;
}

export function isRawData(d: RawData): d is RawData {
    if (!(d instanceof Array)) {
        return false;
    }
    for (const e of d) {
        if (!isRawEntity(e)) {
            return false;
        }
    }

    return true;
}

//------------------------------------------------------------------------------
// Cooked types
//------------------------------------------------------------------------------

export interface Flag {
    readonly kind: "flag";
}

export interface Link {
    value: string,
    tags: string[],
    target?: Entity,
}

export interface LinkList {
    readonly kind: "link";
    readonly links: Link[];
}

export interface Note {
    readonly kind: "note";
    readonly content: string;
}

export type Property = Flag | LinkList | Note;

interface PropertyBag {
    [name: string]: Property | undefined;
}

interface BackLinkBag {
    [name: string]: LinkList | undefined;
}

export interface Entity {
    readonly id: string;
    readonly raw: RawEntity;
    readonly properties: PropertyBag;
    readonly backLinks: BackLinkBag;
}

export interface Index {
    [name: string]: Entity | undefined;
}

export interface MultiIndex {
    [name: string]: Entity[] | undefined;
}

export interface Data {
    readonly byId: Index;
    readonly byProperty: { [property: string]: MultiIndex }
}

//------------------------------------------------------------------------------
// Factories

function makeEntity(raw: RawEntity): Entity {
    return {
        id: raw.id,
        raw: raw,
        properties: {},
        backLinks: {},
    }
}

function makeFlag(): Flag {
    return { kind: "flag" };
}

function makeLinkList(): LinkList {
    return { kind: "link", links: [] };
}

function makeLink(v: string, ts: string[], t?: Entity): Link {
    return { value: v, tags: ts, target: t };
}

function makeNote(content: string): Note {
    return { kind: "note", content: content };
}

//------------------------------------------------------------------------------
// Conversion

export function buildData(rawData: RawData): Data {
    const cooked: Data = {
        byId: {},
        byProperty: {},
    };

    // create all the entities
    for (const re of rawData) {
        const e = makeEntity(re);
        cooked.byId[e.id] = e;
    }

    // build links
    for (const id of Object.keys(cooked.byId)) {
        const e = cooked.byId[id]!; // we know id appears in the dictionary

        for (const pn of Object.keys(e.raw)) {
            if (pn === "id") {
                continue;
            }

            if (cooked.byProperty[pn] === undefined) {
                cooked.byProperty[pn] = {};
            }

            const pv = e.raw[pn];
            if (typeof pv === "boolean") {
                addFlag(e, pn, pv, cooked);
            } else if (typeof pv === "string") {
                addLinkOrNote(e, pn, pv, [], cooked);
            } else if (pv instanceof Array) {
                for (const v of pv) {
                    addLinkOrNote(e, pn, v, [], cooked);
                }
            } else if (typeof pv === "object") {
                const tpv = pv as RawTaggedProperties;

                for (const k of Object.keys(pv)) {
                    const rts = tpv[k];
                    let ts: string[];
                    if (typeof rts === "boolean") {
                        if (!rts) {
                            continue; // skip false tags
                        }

                        ts = [];
                    } else if (typeof rts === "string") {
                        ts = [ rts ];
                    } else {
                        ts = [];
                        for (const t of rts) {
                            ts.push(t);
                        }
                    }

                    addLinkOrNote(e, pn, k, ts, cooked);
                }
            } else {
                throw `Invalid property '${id}.${pn}' = '${pv}' (${typeof pv})`;
            }
        }
    }

    return cooked;
}

export function addFlag(e: Entity, name: string, value: boolean, data: Data) {
    if (!value) {
        return;
    }

    e.properties[name] = makeFlag();

    addToIndex(e, name, String(true), data);
}

function addLinkOrNote(e: Entity, name: string, value: string, tags: string [], data: Data) {
    if (value === "") {
        return;
    }

    // for the moment notes are never used, until I figure out a good way to
    // encode them (also a reason why a dangling link is not sufficient)

    const target = data.byId[value];

    let ll = e.properties[name];
    if (!ll) {
        ll = makeLinkList();
        e.properties[name] = ll;
    } else if (ll.kind !== "link") {
        throw `${e.id}.${name} is a '${ll.kind}' but attempting to use as a link`;
    }
    ll.links.push(makeLink(value, tags, target));

    if (target) {
        // add the backlink
        let bl = target.backLinks[name];
        if (!bl) {
            bl = makeLinkList();
            target.backLinks[name] = bl;
        }
        bl.links.push(makeLink(e.id, tags, e));
    }

    addToIndex(e, name, value, data);
}

function addToIndex(e: Entity, name: string, value: string, data: Data) {
    let i = data.byProperty[name];
    if (!i) {
        i = {};
        data.byProperty[name] = i;
    }

    let c = i[value];
    if (!c) {
        c = [];
        i[value] = c;
    }

    c.push(e);
}

export function removeProperty(e: Entity, name: string, data: Data) {
    const oldValue = e.properties[name];
    if (!oldValue) {
        return;
    }

    e.properties[name] = undefined;

    switch (oldValue.kind) {
        case "flag": {
            removeFromIndex(e, name, String(true), data);
        } break;
        case "link": {
            for (const l of oldValue.links) {
                if (l.target) {
                    removeBacklink(l.target, name, e.id, data);
                }
                removeFromIndex(e, name, l.value, data);
            }
        } break;
        case "note": {
            removeFromIndex(e, name, oldValue.content, data);
        } break;
    }
}

function removeFromIndex(e: Entity, name: string, value: string, data: Data) {
    const i = data.byProperty[name]!;
    const c = i[value]!;

    c.splice(c.indexOf(e), 1);
}

function removeBacklink(e: Entity, name: string, sourceId: string, data: Data) {
    const sources = e.backLinks[name]!;
    for (let i = 0; i < sources.links.length; ++i) {
        const l = sources.links[i];
        if (l.value === sourceId) {
            sources.links.splice(i, 1);
            break;
        }
    }
}

//------------------------------------------------------------------------------
// Extensions
//------------------------------------------------------------------------------

export type Navigator = (target: string) => void;
export type CustomRenderer = (p: Property, e: Entity, n: Navigator) => HTMLElement | null;
export interface CustomRendererDictionary {
    [p: string]: CustomRenderer | undefined;
}
export interface PropertySortDefinition {
    [p: number]: string | string[] | undefined;
    ignore?: string | string[];
}

export interface ExtensionDefinition {
    propertySorting?: PropertySortDefinition;
    hideInSidebar?: string[];
    customRenderers?: CustomRendererDictionary;
}


export interface Extension {
    propertyToPriority: (p: string) => number | "ignore";
    customRenderers: CustomRendererDictionary;

    hideInSidebar: { [property: string]: boolean };
}

export function buildExtension(def?: ExtensionDefinition): Extension {
    if (!def) {
        return { propertyToPriority: () => 0, customRenderers: {}, hideInSidebar: {} };
    }

    const map: { [p: string]: number | "ignore" } = {};
    const mapFn = (p: string) => map[p] || 0;

    if (def.propertySorting) {
        for (const priorityKey of Object.keys(def.propertySorting)) {
            if (priorityKey === "ignore") { continue; }

            const priority = Number(priorityKey); // this should be safe, see the definition of propertySorting

            const sortDef = def.propertySorting[priority];

            if (typeof sortDef === "string") {
                map[sortDef] = priority;
            } else if (sortDef instanceof Array) {
                for (const p of sortDef) {
                    map[p] = priority;
                }
            } else {
                throw `'${sortDef}' in ExtensionDefinition.propertySorting[${priority}] is not a string or string[]`;
            }
        }

        const ignores = def.propertySorting.ignore;
        if (!ignores) {
            // do nothing
        } else if (typeof ignores === "string")
        {
            map[ignores] = "ignore";
        } else if (ignores instanceof Array) {
            for (const p of ignores) {
                map[p] = "ignore";
            }
        } else {
            throw `'${ignores}' in ExtensionDefinition.propertySorting.ignore is not a string or string[]`;
        }
    }

    let hideInSidebar: { [property: string]: boolean } = {};
    if (def.hideInSidebar) {
        def.hideInSidebar.forEach((property: string) => {
            hideInSidebar[property] = true;
        });
    }

    const renderers = def.customRenderers || {};

    return { propertyToPriority: mapFn, customRenderers: renderers, hideInSidebar: hideInSidebar };
}

//------------------------------------------------------------------------------
// Utils
//------------------------------------------------------------------------------

function assertNever(x: never): never {
    throw new Error("Unexpected object: " + x);
}
