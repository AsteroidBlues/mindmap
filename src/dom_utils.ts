function addCssClasses(e: HTMLElement, cssClasses?: string[]) {
    if (cssClasses && cssClasses.length > 0) {
        e.classList.add(...cssClasses);
    }
}

export function button(
    txt?: string,
    onClick?: (this: GlobalEventHandlers, ev: MouseEvent) => any,
    cssClasses?: string[]
): HTMLButtonElement {
    const e = document.createElement("button");
    addCssClasses(e, cssClasses);

    e.type = "button"; // apparently it is necessary, see https://www.w3schools.com/tags/tag_button.asp
    e.innerText = txt || "";
    e.onclick = onClick || null;

    return e;
}

export function checkbox(checked?: boolean, cssClasses?: string[]): HTMLInputElement {
    const e = document.createElement("input");
    addCssClasses(e, cssClasses);

    e.type = "checkbox";
    e.checked = checked || false;

    return e;
}

export function div(children?: HTMLElement[], id?: string, cssClasses?: string[]): HTMLDivElement {
    const e = document.createElement("div");
    addCssClasses(e, cssClasses);

    e.id = id || "";
    for (let child of children || []) {
        e.appendChild(child);
    }

    return e;
}

export function horizontalRule(cssClasses?: string[]): HTMLHRElement {
    const e = document.createElement("hr");
    addCssClasses(e, cssClasses);

    return e;
}

export function link(
    txt: string,
    onClick?: (this: GlobalEventHandlers, ev: MouseEvent) => any,
    cssClasses?: string[]
): HTMLAnchorElement {
    const e = document.createElement("a");
    addCssClasses(e, cssClasses);

    e.innerText = txt;
    e.onclick = onClick || null;

    return e;
}

export function list(cssClasses?: string[]): HTMLUListElement {
    const e = document.createElement("ul");
    addCssClasses(e, cssClasses);

    return e;
}

export function listElement(cssClasses?: string[]): HTMLLIElement {
    const e = document.createElement("li");
    addCssClasses(e, cssClasses);

    return e;
}

export function newLine(cssClasses?: string[]): HTMLBRElement {
    const e = document.createElement("br");
    addCssClasses(e, cssClasses);

    return e;
}

export function numberField(min: number, max: number, cssClasses?: string[]): HTMLInputElement {
    const e = document.createElement("input");
    addCssClasses(e, cssClasses);

    e.type = "number";
    e.min = String(min);
    e.max = String(max);

    return e;
}

export function option(txt: string, cssClasses?: string[]): HTMLOptionElement {
    const e = document.createElement("option");
    addCssClasses(e, cssClasses);

    e.innerText = txt;

    return e;
}

export function paragraph(txt?: string, cssClasses?: string[]): HTMLParagraphElement {
    const e = document.createElement("p");
    addCssClasses(e, cssClasses);

    if (txt) {
        e.innerText = txt;
    }

    return e;
}


export function pre(txt: string, cssClasses?: string[]): HTMLPreElement {
    const e = document.createElement("pre");
    addCssClasses(e, cssClasses);

    e.innerText = txt;

    return e;
}

export function select(cssClasses?: string[]): HTMLSelectElement {
    const e = document.createElement("select");
    addCssClasses(e, cssClasses);

    return e;
}

export function slider(min: number, max: number, initial: number, cssClasses?: string[]): HTMLInputElement {
    const e = document.createElement("input");
    addCssClasses(e, cssClasses);

    e.type = "range";
    e.min = String(min);
    e.max = String(max);
    e.value = String(initial);

    return e;
}

export function span(cssClasses?: string[]): HTMLSpanElement {
    const e = document.createElement("span");
    addCssClasses(e, cssClasses);

    return e;
}

export function text(txt: string, cssClasses?: string[]): HTMLSpanElement {
    const e = span(cssClasses);

    e.innerText = txt;

    return e;
}

export function textBox(txt?: string, cssClasses?: string[]): HTMLInputElement {
    const e = document.createElement("input");
    addCssClasses(e, cssClasses);

    e.type = "text";

    if (txt) {
        e.value = txt;
    }

    return e;
}

//------------------------------------------------------------------------------

function tableCell(type: "th", data?: HTMLElement, cssClasses?: string[]): HTMLTableHeaderCellElement;
function tableCell(type: "td", data?: HTMLElement, cssClasses?: string[]): HTMLTableCellElement;
function tableCell(type: string, data?: HTMLElement, cssClasses?: string[]): HTMLElement {
    const e = document.createElement(type);
    addCssClasses(e, cssClasses);

    if (data) {
        e.appendChild(data);
    }

    return e;
}

export function tableHeaderCell(data?: HTMLElement, cssClasses?: string[]): HTMLTableHeaderCellElement {
    return tableCell("th", data, cssClasses);
}

export function tableHeader(cells: HTMLElement[], cssClasses?: string[]): HTMLTableRowElement {
    const e = document.createElement("tr");

    for (let cell of cells) {
        e.appendChild(tableHeaderCell(cell, cssClasses));
    }

    return e;
}

export function tableRowCell(data?: HTMLElement, cssClasses?: string[]): HTMLTableCellElement {
    return tableCell("td", data, cssClasses);
}

export function tableRow(cells: HTMLElement[], cssClasses?: string[]): HTMLTableRowElement {
    const e = document.createElement("tr");

    for (let cell of cells) {
        e.appendChild(tableRowCell(cell, cssClasses));
    }

    return e;
}

export function table(cssClasses?: string[]): HTMLTableElement {
    const e = document.createElement("table");
    addCssClasses(e, cssClasses);

    return e;
}

//------------------------------------------------------------------------------

export function removeChildren(e: HTMLElement): void {
    while (e.firstChild) {
        e.removeChild(e.firstChild);
    }
}
